<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth.basic'])->group(function () {
    Route::get('account','AccountController@index');
    Route::get('account/check/{account_number}','AccountController@show');
    Route::get('account/total','AccountController@totalBalance');
    Route::post('deposit','TransactionController@deposit');
    Route::post('withdraw','TransactionController@withdraw');
    Route::post('transfer','TransactionController@transfer');
    Route::get('history','TransactionController@index');
});
