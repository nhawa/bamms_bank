<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('login','Auth\LoginController@login');
Route::middleware('auth.basic')->post('logout','Auth\LoginController@logout')->name('logout');
Route::post('register','Auth\RegisterController@register');

Route::get('/login',function(){
    return view('auth');
})->name('login');
Route::get('/register',function(){
    return view('auth');
})->name('register');



Route::middleware(['auth'])->get('/{any}', function(){
    return view('index');
})->where('any', '.*');

