# bamms-bank


simple bank application with Laravel, MySQL, and PHP on server side and HTML, CSS, javascript, and VUE on client side.
accomplished with PhpStorm, Chrome, and Git.

Suggestions
- every account type have different benefit
- client can update their account
- client can see their balance
- validate every transaction
- client can download mutation report 
