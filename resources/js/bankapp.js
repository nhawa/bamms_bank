import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'

Vue.use(VueRouter);
window.axios = axios;
window.axios.defaults.headers.common['Accept'] = 'application/json'; // for all requests
let basic = localStorage.getItem('basic');
if (basic) {
    window.axios.defaults.headers.common['Authorization'] = basic; // for all requests
}

import App from './components/App'
import Auth from './components/auth/Auth'
import Dashboard from './components/bank/Dashboard'
import Transaction from './components/bank/Transaction'
import Deposit from './components/bank/transaction/Deposit'
import Transfer from './components/bank/transaction/Transfer'
import Withdraw from './components/bank/transaction/Withdraw'
import History from './components/bank/transaction/History'
import Login from './components/auth/Login'
import Register from './components/auth/Register'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
        },
        {
            path: '/transaction',
            name: 'transaction',
            component: Transaction,
            children:[
                {
                    path: '/deposit',
                    name: 'deposit',
                    component: Deposit,
                },
                {
                    path: '/withdraw',
                    name: 'withdraw',
                    component: Withdraw,
                },
                {
                    path: '/transfer',
                    name: 'transfer',
                    component: Transfer,
                },
                {
                    path: '/History',
                    name: 'history',
                    component: History,
                },
            ]
        },
    ],
});
const app = new Vue({
    el: '#app',
    components: { App,Auth },
    router,
});