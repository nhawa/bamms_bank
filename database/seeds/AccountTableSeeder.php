<?php

use Illuminate\Database\Seeder;

class AccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account')->insert([
            'user_id'           => 1,
            'account_number'    => rand(1000000,9999999),
            'type'              => 1,
            'description'       => 'description',
            'balance'           => 10000000,
        ]);
    }
}
