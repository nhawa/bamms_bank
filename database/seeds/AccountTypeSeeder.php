<?php

use Illuminate\Database\Seeder;

class AccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('account_type')->insert([
            [
                'id' => 1,
                'name' => 'Gold'
            ],[
                'id' => 2,
                'name' => 'Silver'
            ],[
                'id' => 3,
                'name' => 'Bronze'
            ]
        ]);
    }
}
