<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'        => 1,
            'name'      => 'administrator',
            'email'     => 'admin@gmail.com',
            'password'  => bcrypt('admin123'),
            'phone'     => '081343527951',
            'address'   => 'anywhere',
            'level'     => 1
        ]);
    }
}
