<?php

namespace App\Http\Controllers\Auth;

use App\Model\Account;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\AppException
     */
    public function register(Request $request)
    {
        $this->validator($request->all());


        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return [
            'status' => true,
            'message' => 'success register',
            'data' => [
                'redirect_to'   => $this->redirectTo,
                'basic'         => 'Basic '.base64_encode($request->email.':'.$request->password),
                'user'          => auth()->user()
            ]
        ];
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return void
     * @throws \App\Exceptions\AppException
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        $this->validate($validator);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->name     = $data['name'];
        $user->phone    = $data['phone'];
        $user->address  = $data['address'];
        $user->email    = $data['email'];
        $user->password = Hash::make($data['password']);
        if ($user->save()){
            $do = true;
            while ($do){
                $code = rand(1000000,9999999);
                if (is_null(Account::select('id')->where('account_number',$code)->first()))
                    $do = false;
            }
            $account = new Account();
            $account->user_id = $user->id;
            $account->account_number = $code;
            $account->type = 3;
            $account->description = 'new account';
            $account->balance = 0;
            $account->save();
        }
        return $user;
    }
}
