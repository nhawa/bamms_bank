<?php

namespace App\Http\Controllers;

use App\Exceptions\AppException;
use App\Model\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    /**
     * @return array
     * @throws AppException
     */
    public function index(){
        $account =  Account::select('account_number', 'type', 'description', 'balance')
            ->with('type')
            ->where('user_id',auth()->user()->id)
            ->first();
        if(is_null($account)){
            throw AppException::noData();
        }
        return $this->response($account,'success retrieve account');
    }

    public function totalBalance(){
        if (auth()->user()->level==1){
            $total = Account::select(DB::raw('sum(balance) as total'))->first()->total;
        }else{
            $total = 0;
        }
        return $this->response($total,'success retrieve total');
    }

    /**
     * @param $account_number
     * @return mixed
     * @throws AppException
     */
    public function show($account_number){
        $this->validator(compact('account_number'));
        $account = Account::select('account.*','users.name')->where('account_number',$account_number)
            ->join('users','users.id','account.user_id')
            ->first();
        if (is_null($account)){
            throw AppException::noData();
        }else{
            return $this->response($account,'success retrieve account');
        }
    }

    /**
     * @param array $data
     * @throws \App\Exceptions\AppException
     */
    protected function validator(array $data){
        $validator = Validator::make($data, [
            'account_number' => ['required', 'numeric'],
        ]);
        $this->validate($validator);
    }
}
