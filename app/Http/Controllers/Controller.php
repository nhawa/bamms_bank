<?php

namespace App\Http\Controllers;

use App\Exceptions\AppException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param $validator
     * @throws AppException
     */
    protected function validate(Validator $validator){
        if ($validator->fails()) {
            throw AppException::invalidRequest($validator->getMessageBag()->first());
        }
    }

    protected function response($data,$message = ''){
        return [
            'status'  => true,
            'message' => $message,
            'data'    => $data
        ];
    }

    protected function responseFailed($data,$message=''){
        return [
            'status'  => false,
            'message' => $message,
            'data'    => $data
        ];
    }
}
