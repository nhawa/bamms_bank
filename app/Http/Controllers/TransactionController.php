<?php

namespace App\Http\Controllers;

use App\Exceptions\AppException;
use App\Model\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    /**
     * @return array
     * @throws AppException
     */
    public function index(){
        $transaction = Transaction::listTransaction();

        if (!$transaction->isEmpty())
            return $this->response($transaction,'success');
        else
            throw AppException::noData();
    }
    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\AppException
     */
    public function deposit(Request $request){
        $this->transactionValidator($request->all());
        return $this->doTransaction($request,'in','deposit');
    }
    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\AppException
     */
    public function withdraw(Request $request){
        $this->transactionValidator($request->all());
        return $this->doTransaction($request,'out','withdraw');
    }

    /**
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\AppException
     */
    public function transfer(Request $request){
        $this->transferValidator($request->all());

        if ($transfer = Transaction::saveTransaction([
            'type'      => 'out',
            'name'      => 'transfer',
            'amount'    => $request->amount
        ],$request->account_number)){
            info('transfer outgoing is '.$transfer);
            $transfer = Transaction::saveTransaction([
                'type'      => 'in',
                'name'      => 'transfer',
                'amount'    => $request->amount
            ],$request->account_number);
            info('transfer incoming is '.$transfer);
        }
        if ($transfer)
            return $this->response('success transfer','success');
        else
            return $this->response('fail transfer','failed');

    }

    /**
     * @param Request $request
     * @param $type
     * @param $name
     * @return array
     */
    protected function doTransaction(Request $request, $type, $name){
        if (Transaction::saveTransaction([
            'type'      => $type,
            'name'      => $name,
            'amount'    => $request->amount
        ]))
            return $this->response('success '.$name,'success');
        else
            return $this->responseFailed('fail '.$name,'failed');
    }
    /**
     * @param array $data
     * @throws \App\Exceptions\AppException
     */
    protected function transactionValidator(array $data){
        $validator = Validator::make($data, [
            'amount' => ['required', 'numeric'],
        ]);
        $this->validate($validator);
    }

    /**
     * @param array $data
     * @throws \App\Exceptions\AppException
     */
    private function transferValidator(array $data)
    {
        $validator = Validator::make($data, [
            'amount'            => ['required', 'numeric'],
            'account_number'    => ['required', 'numeric','exists:account,account_number'],
        ]);
        $this->validate($validator);
    }
}
