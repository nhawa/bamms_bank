<?php

namespace App\Model;

use App\Exceptions\AppException;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //init
    protected $table = 'transaction';

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function account(){
        return $this->hasOne('App\Model\Account','account_number','account_number');
    }
    //end init

    //retrieve
    public static function listTransaction()
    {
        $transaction = self::with([
            'user' => function($q){
                $q->select('id','name');
            },
            'account' => function($q){
                $q->select('account_number','users.name');
                $q->join('users','users.id','account.user_id');
            }
        ])->orderBy('id','desc');
        if (auth()->user()->level == 1){
            return $transaction->get();
        }else{
            return $transaction->where('user_id',auth()->user()->id)->get();
        }
    }
    //end retrieve

    //save
    /**
     * @param $data
     * @param null $account_number
     * @return bool
     * @throws AppException
     */
    public static function saveTransaction($data, $account_number = null){
        if ($data['type'] == 'in' && $data['name'] == 'transfer'){
            $account = self::getAccount(null,$account_number);
            $user_id = $account->user_id;
            $account_sender = self::getAccount(auth()->user()->id);
            $account_number = $account_sender->account_number;
        }else{
            $user_id = auth()->user()->id;
            $account = self::getAccount($user_id);
        }

        if ($data['type']=='out' && $account->balance < $data['amount']){
            throw AppException::invalidRequest('insufficient balance');
        }

        $transaction = new self();
        $transaction->type              = $data['type'];
        $transaction->name              = $data['name'];
        $transaction->user_id           = $user_id;
        $transaction->account_number    = $account_number;
        $transaction->amount            = $data['amount'];
        $transaction->balance           = $data['type']=='in' ? $account->balance + $data['amount'] : $account->balance - $data['amount'];
        if ($status = $transaction->save()){
            $account->balance = $transaction->balance;
            $status = $account->save();
            info('update account is '.$status);
        }
        return $status;
    }

    private static function getAccount($user_id, $account_number = null){
        if (is_null($account_number))
            return Account::select('id','account_number','balance')->where('user_id',$user_id)->first();
        else
            return Account::select('id','account_number','balance','user_id')->where('account_number',$account_number)->first();
    }
    //end save
}
