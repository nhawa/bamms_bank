<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'account';

    public function type(){
        return $this->hasOne('App\Model\AccountType','id','type');
    }
}
