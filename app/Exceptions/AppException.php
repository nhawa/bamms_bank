<?php

namespace App\Exceptions;


use Illuminate\Support\Facades\Lang;

class AppException extends \Exception
{
    /**
     * @var int
     */
    private $httpStatusCode;

    /**
     * @var string
     */
    private $errorType;

    /**
     * @var null|string
     */
    private $hint;

    /**
     * @var null|string
     */
    private $redirectUri;


    /**
     * Throw a new exception.
     *
     * @param string      $message        Error message
     * @param int         $code           Error code
     * @param string      $errorType      Error type
     * @param int         $httpStatusCode HTTP status code to send (default = 400)
     * @param null|string $hint           A helper hint
     * @param null|string $redirectUri    A HTTP URI to redirect the user back to
     */
    public function __construct($message, $code, $errorType, $httpStatusCode = 400, $hint = null, $redirectUri = null)
    {
        parent::__construct($message, $code);
        $this->httpStatusCode = $httpStatusCode;
        $this->errorType = $errorType;
        $this->hint = $hint;
        $this->redirectUri = $redirectUri;
    }

    /**
     * Invalid request error.
     *
     * @param string      $parameter The invalid parameter
     * @param null|string $hint
     *
     * @return static
     */
    public static function invalidRequest($message = null,$httpSttsCode = 400)
    {
        $errorMessage = 'The request is missing a required parameter, includes an invalid parameter value, ' .
            'includes a parameter more than once, or is otherwise malformed.';
        $hint = ($message == null) ? $errorMessage: $message;
        return new static($hint, 3, 'invalid_request', $httpSttsCode);
    }

    public static function info($message){
        return new static($message,2,'info',300);
    }

    /**
     * Invalid client error.
     *
     * @return static
     */
    public static function invalidClient()
    {
        $errorMessage = 'Client authentication failed';

        return new static($errorMessage, 4, 'invalid_client', 401);
    }

    public static function invalidVerifyTime()
    {
        $errorMessage = 'Verification code already send to your email. Please wait a view minute to resend code';

        return new static($errorMessage, 4, 'resend_code_failed', 400);
    }

    public static function invalidVerifyCode()
    {
        $errorMessage = 'Invalid verification code. Please check your code or contact our customer service';

        return new static($errorMessage, 4, 'invalid_code', 400);
    }
    public static function transactionFail()
    {
        $errorMessage = Lang::get('error.transaction_fail');

        return new static($errorMessage, 5, 'trnsaction_fail', 400);
    }
    public static function noData()
    {
        $errorMessage = Lang::get('error.no_data.data');

        return new static($errorMessage, 4, 'no_content', 204);
    }
    public static function noDataDate()
    {
        $errorMessage = Lang::get('error.no_data.date');
        return new static($errorMessage, 4, 'no_content', 204);
    }
    public static function bookingCannotBeProcessed($errorMessage=null)
    {
        if (is_null($errorMessage))
            $errorMessage = Lang::get('error.bookingCannotBeProcessed');
        return new static($errorMessage, 5, 'order_error', 400);
    }
    public static function paymentCannotBeProcessed($errorMessage=null)
    {
        if (is_null($errorMessage))
            $errorMessage = Lang::get('error.paymentCannotBeProcessed');
        return new static($errorMessage, 5, 'payment_error', 400);
    }
    public static function bookingExpired($errorMessage=null)
    {
        if (is_null($errorMessage))
            $errorMessage = Lang::get('error.bookingExpired');
        return new static($errorMessage, 3, 'expired', 300);
    }
    public static function cancellationFail($errorMessage=null)
    {
        if (is_null($errorMessage))
            $errorMessage = Lang::get('error.cancellationFail');
        return new static($errorMessage, 5, 'payment_error', 400);
    }


    /**
     * Invalid credentials error.
     *
     * @return static
     */
    public static function invalidCredentials()
    {
        return new static('The user credentials were incorrect.', 6, 'invalid_credentials', 401);
    }

    /**
     * Server error.
     *
     * @param $hint
     *
     * @return static
     *
     * @codeCoverageIgnore
     */
    public static function serverError($hint)
    {
        return new static(
            'The authorization server encountered an unexpected condition which prevented it from fulfilling'
            . ' the request. hint: ' . $hint,
            7,
            'server_error',
            500
        );
    }

    /**
     * Access denied.
     *
     * @param null|string $hint
     * @param null|string $redirectUri
     *
     * @return static
     */
    public static function accessDenied($hint = null, $redirectUri = null)
    {
        return new static(
            'The resource owner or authorization server denied the request.',
            9,
            'access_denied',
            401,
            $hint,
            $redirectUri
        );
    }


    /**
     * @return string
     */
    public function getErrorType()
    {
        return $this->errorType;
    }

    /**
     * Get all headers that have to be send with the error response.
     *
     * @return array Array with header values
     */
    public function getHttpHeaders()
    {
        $headers = [
            'Content-type' => 'application/json',
        ];

        // Add "WWW-Authenticate" header
        //
        // RFC 6749, section 5.2.:
        // "If the client attempted to authenticate via the 'Authorization'
        // request header field, the authorization server MUST
        // respond with an HTTP 401 (Unauthorized) status code and
        // include the "WWW-Authenticate" response header field
        // matching the authentication scheme used by the client.
        // @codeCoverageIgnoreStart
        if ($this->errorType === 'invalid_client') {
            $authScheme = 'Basic';
            if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER) !== false
                && strpos($_SERVER['HTTP_AUTHORIZATION'], 'Bearer') === 0
            ) {
                $authScheme = 'Bearer';
            }
            $headers['WWW-Authenticate'] = $authScheme . ' realm="OAuth"';
        }
        // @codeCoverageIgnoreEnd
        return $headers;
    }

    /**
     * Returns the HTTP status code to send when the exceptions is output.
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * @return null|string
     */
    public function getHint()
    {
        return $this->hint;
    }

}
